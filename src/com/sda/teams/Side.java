package com.sda.teams;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;

public enum Side {
    EVIL,
    GOOD,
    UNKNOWN;

    public boolean belongsTo(AbstractHero hero) {

        switch (this) {
            case EVIL:
                return hero instanceof Villain;
            case GOOD:
                return hero instanceof SuperHero;
            case UNKNOWN:
            default:
                return false;
        }
    }
}

