package com.sda.teams;

import com.sda.superheroes.AbstractHero;

public class InvalidHeroTeamException extends Exception {

    public InvalidHeroTeamException(String message) {
        super(message);
    }

    public InvalidHeroTeamException(AbstractHero hero) {
        super("There was an exception with hero called " + hero.getName());
    }

    public InvalidHeroTeamException(Team team) {
        super("There was an exception with team color " + team);
    }
}
