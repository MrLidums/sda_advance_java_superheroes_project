package com.sda.teams;

import com.sda.superheroes.*;
import com.sda.utils.HeroCreator;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Team implements Comparable<Team> {
    static String heroNames[] = {"Superman", "Batman", "Spiderman", "Thor", "Capitan America", "Wonder Woman", "The Thing", "Mr Fantastic", "Iron Man", "Wally West", "Barry Allen", "Invisible Woman", "Human Torch"};
    static String villainNames[] = {"Joker", "Lex Luthor", "Magneto", "Dr. Doom", "Galactus", "Darkseid", "Ra's al Ghul", "Loki", "Kingpin", "Catwoman", "Two-face", "Norman Osborn", "Red Skull", "Siniestro"};
    static String suffixes[] = {"", " the big", " the small", " the best", " the terrible", " the pain giver", " the amazing"};

    private TeamType teamType;
    private List<AbstractHero> heroesList;
    private AbstractHero teamLeader;
    private Side side = Side.UNKNOWN;
    private boolean buff;


    public Team(TeamType teamType) {
        this.teamType = teamType;
        this.heroesList = new ArrayList<AbstractHero>();
    }

    public static Team getTeamOf(int number, TeamType teamType) throws InvalidHeroTeamException{

        Team team = new Team(teamType);

        for (int i = 0; i < number; i++) {

            switch (HeroType.randomHeroType()){

                case HERO:
                    team.addHeroToTeam(creatRandomHero(teamType,HeroType.HERO));
                    break;

                case VILLAIN:
                    team.addHeroToTeam(creatRandomHero(teamType,HeroType.VILLAIN));
                    break;
            }
        }
       return team;
    }

    private static AbstractHero creatRandomHero(TeamType teamType,HeroType heroType){
        //todo choose name for hero and villain differently
        int health = new Random().nextInt(50) + 101;
        int attack = new Random().nextInt(50) + 101;
        int defense = new Random().nextInt(50) + 101;

        int nameIndex = new Random().nextInt(heroNames.length);
        int suffixIndex = new Random().nextInt(suffixes.length);

        String heroName = teamType.name()+ " " + heroNames[nameIndex] + suffixes[suffixIndex];

        HeroStatistics stats = new HeroStatistics(health,attack,defense);

        return HeroCreator.createHero(heroType,heroName,stats,teamType);
    }

    public void addHeroToTeam(AbstractHero hero) throws InvalidHeroTeamException {

        //Modify addHeroToTeam so now it returns *void* instead of *boolean*
        // but throws and exception if you try to add a member to the wrong team
        // 1. check if the team type of hero is equals to the team type of this team
        boolean sameTeam = this.teamType.equals(hero.getTeam());
        // 2. check if the hero already exists in the team
        boolean isInTeam = this.heroesList.contains(hero);

        // if all it's ok: add the hero to the list and return true
        if (sameTeam && !isInTeam) {
            this.heroesList.add(hero);
            //==========================================================
            //this is good practise to check Optional before assigne
            Optional<AbstractHero> teamLeader = findTeamLeader();
            if (teamLeader.isPresent()) {
                this.teamLeader = teamLeader.get();
            }
            //==========================================================
            this.updateTeamSide();
//            return true;
        } else if (isInTeam) {
            // if hero is in the same team, but already present in it,
            // return false and don't add the hero to the list
//            System.out.println("This hero is already in the team");
            throw new InvalidHeroTeamException("This hero is already in the team");
        } else {
            // if not, return false and don't add the hero to the list
//            System.out.println("This hero can not be added to this team");
            throw new InvalidHeroTeamException("This hero can not be added to this team");
        }
    }

    public Optional<AbstractHero> findTeamLeader() {

        Optional<AbstractHero> batman = this.heroesList.stream()
                .filter(hero -> hero.getName().equals("Batman"))
                .findFirst();

        if (batman.isPresent()) {
            return batman;
        } else {
            return this.heroesList.stream().max(Comparator.comparing(AbstractHero::getPower));
        }
    }

    public boolean isBuff() {
        return buff;
    }

    public TeamType getTeamType() {
        return teamType;
    }

    public List<AbstractHero> getHeroesList() {
        return heroesList;
    }

    public AbstractHero getTeamLeader() {
        return teamLeader;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    @Override
    public String toString() {

        String text = "Team: " + teamType + "\n";

        for (AbstractHero h : heroesList) {
            if (h.equals(this.teamLeader)) {
                text += h.getName() + " [leader]" + "\n";
            } else {
                text += h.getName() + "\n";
            }
        }
        return text;
    }

    public String toPrint() {

        return heroesList.stream()
                .map(p -> {
                    return p.equals(teamLeader) ? p.getName() + " [leader]" : p.getName();
                })
                .reduce("", (currValue, element) -> {
                    if (currValue.equals("")) {
                        return "" + element;
                    } else {
                        return currValue + "\n" + element;
                    }
                });
    }

    public double getTeamPower() {
        return heroesList.stream()
                .mapToDouble(AbstractHero::getPower)
                .sum();
    }

    public void increaseTeamPowerWithAdditionalBuff() throws InvalidHeroTeamException {
        //which will raise the strength of the team after strengthening based on the formula:
        //[for each: if-> Villain-> addToHealth (10); if-> SuperHero-> addToDefense (10)]
        int buff = 10;

        if (!isBuff()) {
            this.buff = true;
            heroesList.stream().forEach(hero -> {
                if (hero instanceof SuperHero) {
                    hero.getStats().addDefense(buff);
                } else {
                    hero.getStats().addHealth(buff);
                }
            });
        } else {

            throw new InvalidHeroTeamException(this);
        }
    }

    @Override
    public int compareTo(Team otherTeam) {

        double result = this.getTeamPower() - otherTeam.getTeamPower();

        return (int) Math.round(result);
    }

    //Create numberOfAliveMembers() in Team returning the number of members that are alive.
    //- Create aliveMembers() in Team returning the members that are alive
    //- Create chooseAliveMember() in Team, getting a member that is alive (Optional type)

    private void updateTeamSide() {

        //updateTeamSide() decides the side based on
        // if the sum of the powers of the villains is higher (EVIL)
        // or lower (GOOD) than the power together of the SuperHeroes
        long villainPower = (long) heroesList.stream()
                .filter(hero -> hero instanceof Villain)
                .mapToDouble(AbstractHero::getPower)
                .sum();

        long heroesPower = (long) heroesList.stream()
                .filter(hero -> hero instanceof SuperHero)
                .mapToDouble(AbstractHero::getPower)
                .sum();

        if (heroesPower > villainPower) {
            setSide(Side.GOOD);
        } else if (villainPower > heroesPower) {
            setSide(Side.EVIL);
        } else {
            setSide(Side.UNKNOWN);
        }
    }

    public List<AbstractHero> aliveMembers() {
        return heroesList.stream().filter(AbstractHero::isAlive).collect(Collectors.toList());
    }

    public long numberOfAliveMembers() {
        return aliveMembers().size();
    }

    //create a static method getTeamOf(int number, TeamType teamType),
    // where you create and return a team of this number of heroes or villains of that team type (color).
    // For every hero, you will calculate random stats for health, attack and defense between 100 and 150.
    // The decision of whether the hero is superHero or Villain for every one of the members will be random (hint: nextBoolean()).
    // Use the following names to choose the name randomly,
    // and add the teamType before and a suffix after
    // (so the name will be something like "RED Batman the small" or "BLUE Jocker the amazing").

    //teamType + random heroNames + random suffixes
    //teamType + random villainNames + random suffixes

    public Optional<AbstractHero> chooseAliveMember() {
        return aliveMembers().stream().findAny();
    }
}
