package com.sda.utils;

import com.sda.superheroes.AbstractHero;
import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.superheroes.HeroType;
import com.sda.teams.TeamType;

public class HeroCreator {

    private static SuperHero createSuperHero(String name, HeroStatistics stats, TeamType team) {
        return new SuperHero(name, stats, team);
    }

    private static Villain createVillain(String name, HeroStatistics stats, TeamType team) {
        return new Villain(name, stats, team);
    }

    //create hero/player by selecting witch type of hero it will be
    public static AbstractHero createHero(HeroType heroType, String name, HeroStatistics stats, TeamType team) {
        switch (heroType) {
            case VILLAIN:
                return new Villain(name, stats, team);
            case HERO:
                return new SuperHero(name, stats, team);
            default:
                return null;
        }
    }

    public static SuperHero createHeroWithDefaultStats (String name, TeamType team) {
        // 1. load the properties: PropertyReader.loadPropertyValues ();
        HeroStatistics defaultStats = getDefaultStatistics();
        // 5. create the Hero with this HeroStatistics
        return createSuperHero(name,defaultStats,team);
    }

    public static Villain createVillainWithDefaultStats (String name, TeamType team) {
        // 1. Create HeroStatistics object
        HeroStatistics defaultStats = getDefaultStatistics();
        // 2. create the Hero with this HeroStatistics
        return createVillain(name,defaultStats,team);
    }

    private static HeroStatistics getDefaultStatistics() {
        // 1. load the properties: PropertyReader.loadPropertyValues ();
        PropertyReader.loadPropertyValues();
        // 2. get all the properties for health, attack and defense
        int health = Integer.parseInt(System.getProperty("config.superHeroBaseHealth"));
        int attack = Integer.parseInt(System.getProperty("config.superHeroBaseAttack"));
        int defence = Integer.parseInt(System.getProperty("config.superHeroBaseDefence"));
//        System.out.println(System.getProperties());
        // 3. create HeroStatistics with them
        return new HeroStatistics(health, attack, defence);
    }
}
