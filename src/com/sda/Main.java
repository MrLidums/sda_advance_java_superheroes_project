package com.sda;

import com.sda.teams.Side;
import com.sda.teams.TeamType;
import com.sda.utils.HeroCreator;

public class Main {
    public static void main(String [] args) {
        System.out.println(HeroCreator.createHeroWithDefaultStats("Batman", TeamType.GREEN));

    }
}
