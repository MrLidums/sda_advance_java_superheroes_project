package com.sda.superheroes;

import com.sda.teams.TeamType;

public class Villain extends AbstractHero{

    public Villain(String name, HeroStatistics stats, TeamType team) {
        super(name, stats, team);
    }

    @Override
    public double getPower() {
        int attack = this.getStats().getAttack();
        int defense = this.getStats().getDefense();
        int health = this.getStats().getHealth();

        return (attack + health) * defense;
    }

}
