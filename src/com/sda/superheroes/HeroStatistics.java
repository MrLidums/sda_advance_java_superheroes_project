package com.sda.superheroes;

public class HeroStatistics {

	private int health;
	private int attack;
	private int defense;

	public HeroStatistics(int health, int attack, int defense) {
		this.health = health;
		this.attack = attack;
		this.defense = defense;
	}

	public int getHealth() {
		return health;
	}

	public int getAttack() {
		return attack;
	}

	public int getDefense() {
		return defense;
	}

	public void addHealth(int amount){
		this.health += amount;
	}

	public void addAttack(int amount){
		this.attack += amount;
	}

    public void setHealth(int health) {
        this.health = health;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void addDefense(int amount){
		this.defense += amount;
	}

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HeroStatistics){
            HeroStatistics heroStats = (HeroStatistics) obj;
            boolean equalHealth = (this.health == heroStats.getHealth());
            boolean equalAttack = (this.attack == heroStats.getAttack());
            boolean equalDefense = (this.defense == heroStats.getDefense());

            return (equalHealth && equalAttack && equalDefense);
        }
	    return false;
    }

    @Override
    public String toString() {
        return "HeroStatistics{" +
                "health=" + health +
                ", attack=" + attack +
                ", defense=" + defense +
                '}';
    }
}
