package com.sda.superheroes;

import com.sda.teams.TeamType;

public abstract class AbstractHero {

    private String name;
    private HeroStatistics stats;
    private TeamType team;

    public AbstractHero(String name, HeroStatistics stats, TeamType team) {
        this.name = name;
        this.stats = stats;
        this.team = team;

        switch (team) {
            case RED:
                this.stats.addHealth(50);
                break;
            case BLUE:
                this.stats.addAttack(50);
                break;
            case GREEN:
                this.stats.addDefense(50);
                break;
        }

    }

    public String getName() {
        return name;
    }

    public HeroStatistics getStats() {
        return stats;
    }

    //ToDO remove after project is finished
    public void setStats(HeroStatistics stats) {
        this.stats = stats;
    }

    public TeamType getTeam() {
        return team;
    }

    public boolean isAlive() {
        return this.getStats().getHealth() > 0;
    }

    //create a method takeDamage() with a parameter double damage. This method will subtract this damage from the hero's health.
    // If after that, the health of the hero is bigger than zero, it returns true.
    // If hero's health goes to zero or less, it's set to zero, and the method returns false.

    public boolean takeDamage(int damage) {
        int health = this.getStats().getHealth();
        int afterDamage = health - damage;

        if (afterDamage > 0) {
            this.getStats().setHealth(afterDamage);
            return true;
        } else {
            this.getStats().setHealth(0);
            return false;
        }
    }

    public void kill(){
        this.getStats().setHealth(0);
    }

    public abstract double getPower();

    @Override
    public String toString() {
        return "AbstractHero{" +
                "name='" + name + '\'' +
                ", stats=" + stats +
                ", team=" + team +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof AbstractHero) {
            AbstractHero hero = (AbstractHero) obj;

            return this.name.equals(hero.getName())
                    && this.team.equals(hero.getTeam())
                    && this.stats.equals(hero.getStats());

        } else {
            System.out.println("This is NOT a hero");
            return false;
        }
    }

}
