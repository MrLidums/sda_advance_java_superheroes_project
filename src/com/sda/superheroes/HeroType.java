package com.sda.superheroes;

import java.util.Random;

public enum HeroType {
    HERO,
    VILLAIN;

    public static HeroType randomHeroType() {
        return new Random().nextBoolean() ? HERO : VILLAIN;
    }
}
