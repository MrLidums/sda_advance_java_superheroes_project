package com.sda.tests.teams;

import com.sda.superheroes.*;
import com.sda.teams.*;
import com.sda.utils.HeroCreator;
import com.sda.utils.TeamUtils;
import org.junit.*;

import java.util.Arrays;

import static org.junit.Assert.*;

public class TeamTests {

    static HeroStatistics statsSmall, statsBig;
    static Team greenTeam, redTeam, blueTeam;
    static String[] heroNames, villainNames;
    static AbstractHero superHero, villain;
    static SuperHero hero1, hero2, hero3, hero4, hero5;
    static Villain villain1, villain2, villain3, villain4, villain5;
    static AbstractHero[] heroesList, villainsList;

    @BeforeClass
    public static void setup() {

        //stats
        statsSmall = new HeroStatistics(50, 50, 50);
        statsBig = new HeroStatistics(200, 300, 400);

        //lists with names
        heroNames = new String[]{"Batman","Deadpool","Hellboy","Superman","Spiderman"};
        villainNames = new String[]{"Jocker","Penguin","Catwoman","Green Goblin","Mystique"};

    }

    @Before
    public void clearVars(){
        //clear teams
        greenTeam = new Team(TeamType.GREEN);
        redTeam = new Team(TeamType.RED);
        blueTeam = new Team(TeamType.BLUE);

        //superHeroes
        hero1 = HeroCreator.createHeroWithDefaultStats("Batman", TeamType.GREEN);
        hero2 = HeroCreator.createHeroWithDefaultStats("Deadpool", TeamType.GREEN);
        hero3 = HeroCreator.createHeroWithDefaultStats("Hellboy", TeamType.GREEN);
        hero4 = HeroCreator.createHeroWithDefaultStats("Superman", TeamType.GREEN);
        hero5 = HeroCreator.createHeroWithDefaultStats("Spiderman", TeamType.GREEN);
        //array of heroes
        heroesList = new AbstractHero[]{hero1, hero2, hero3, hero4, hero5};

        //villains
        villain1 = HeroCreator.createVillainWithDefaultStats("Jocker", TeamType.RED);
        villain2 = HeroCreator.createVillainWithDefaultStats("Penguin", TeamType.RED);
        villain3 = HeroCreator.createVillainWithDefaultStats("Catwoman", TeamType.RED);
        villain4 = HeroCreator.createVillainWithDefaultStats("Green Goblin", TeamType.RED);
        villain5 = HeroCreator.createVillainWithDefaultStats("Mystique", TeamType.RED);
        //array of villains
        villainsList = new AbstractHero[]{villain1, villain2, villain3, villain4, villain5};

        System.out.println("reseting vars");
    }

    @Test
    public void testLambda(){
//        heroesList = Arrays.stream(heroesList)
//                .map(hero -> new SuperHero(hero.getName(),statsBig,TeamType.GREEN))
//                .collect(Collectors.toList());
    }

    @Test
    public void testAddHeroToTheTeam() {

        //TODO refactor Test to test all cases
        //given
        AbstractHero batman = new SuperHero("Batman", statsSmall, TeamType.GREEN);
        AbstractHero batman2 = new SuperHero("Batman", statsSmall, TeamType.GREEN);
        AbstractHero penguin = new Villain("Mr.Penguin", statsBig, TeamType.RED);

        //when
        boolean resultEqualTrue = false;
        boolean resultEqualFalse = false;

        try {
            redTeam.addHeroToTeam(penguin);
            greenTeam.addHeroToTeam(batman);
//            greenTeam.addHeroToTeam(batman2);
            resultEqualTrue = batman.equals(batman2);
            resultEqualFalse = penguin.equals(batman);
        } catch (InvalidHeroTeamException e) {
            fail("Test failed because AddHeroToTheTeam method threw exception");
        }

        greenTeam.findTeamLeader();
        redTeam.findTeamLeader();

        //then
        assertTrue(resultEqualTrue);
        assertFalse(resultEqualFalse);

    }

    @Test
    public void testTeamLeader() {
        //given hero with big stats
        AbstractHero superman = new SuperHero("Superman", statsBig, TeamType.GREEN);

        try {
            //when Team without Batman, the strongest hero is teamleader
            Arrays.stream(heroesList)
                    .filter(hero -> !hero.getName().equals("Batman"))
                    .forEach(hero -> {
                        try {
                            greenTeam.addHeroToTeam(hero);
                        } catch (InvalidHeroTeamException e) {
                            e.printStackTrace();
                            fail("testTeamLeader could not add hero to the team");
                        }
                    });
            greenTeam.addHeroToTeam(superman);

            //then
            assertEquals(superman, greenTeam.getTeamLeader());

            //when add Batman (hero1), he becomes teamleader
            greenTeam.addHeroToTeam(hero1);
        } catch (InvalidHeroTeamException e) {
            e.printStackTrace();
            fail("testTeamLeader failed");
        }

        System.out.println("Batman power: " + hero1.getPower());
        System.out.println("Superman power: " + superman.getPower());

        //then
        assertEquals(hero1, greenTeam.getTeamLeader());

    }

    @Test
    public void testTeamPower() {

        //given
        Arrays.stream(heroesList).forEach(hero -> {
            try {
                hero.setStats(statsBig);
                greenTeam.addHeroToTeam(hero);
                System.out.printf("Green teams power is: %d%n", (long) greenTeam.getTeamPower());
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testTeamPower failed to add hero to the team");
            }
        });

        Arrays.stream(villainsList).forEach(villain -> {
            try {
                villain.setStats(statsSmall);
                redTeam.addHeroToTeam(villain);
                System.out.printf("Red teams power is: %d%n", (long) redTeam.getTeamPower());
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testTeamPower failed to add villain to the team");
            }
        });

        //when
        long greenPower = (long) greenTeam.getTeamPower();
        long redPower = (long) redTeam.getTeamPower();

        //then
        //750 000	37 500
        assertEquals(700_000, greenPower);
        assertEquals(25_000, redPower);

    }

    @Test
    public void testUpdateTeamStats() {
        // Create a unit test where you create a Team "Gotham", including superhero Batman and villain "Joker".
        // Then call increaseTeamPowerWithAdditionalBuff and
        // check that the values of the herostatistics have updated properly

        /*given*/
        Team gotham = new Team(TeamType.BLUE);
        AbstractHero batman = HeroCreator.createHeroWithDefaultStats("Batman", TeamType.BLUE);
        AbstractHero jocker = HeroCreator.createVillainWithDefaultStats("Joker", TeamType.BLUE);

        try {
            /*when*/
            gotham.addHeroToTeam(batman);
            gotham.addHeroToTeam(jocker);
            gotham.increaseTeamPowerWithAdditionalBuff();
        } catch (InvalidHeroTeamException e) {
            e.printStackTrace();
            fail("testUpdateTeamStats failed");
        }

        /*then*/
        assertTrue(batman.getStats().getDefense() == 110);
        assertTrue(jocker.getStats().getHealth() == 110);
    }

    @Test
    public void testGetStrongestTeam() {
        //Build a unit test that creates two teams, team1 which is very powerful and team2 which is weaker,
        //and use getStrongestTeam to assert that the team1 is equals to the most powerful team

        /*given*/
        Arrays.stream(villainsList).forEach(villain -> {
            try {
                redTeam.addHeroToTeam(villain);
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testGetStrongestTeam failed to add villain to the team");
            }
        });
        Arrays.stream(heroesList).forEach(hero -> {
            try {
                greenTeam.addHeroToTeam(hero);
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testGetStrongestTeam failed to add hero to the team");
            }
        });

        System.out.println("Green power: " + greenTeam.getTeamPower());
        System.out.println("Red power: " + redTeam.getTeamPower());

        /*when*/
        Team strongestTeam = TeamUtils.getStrongestTeam(greenTeam, redTeam);

        /*then*/
        assertEquals(null, strongestTeam);
    }

    @Test
    public void testStrongestTeamAfterTeamStatsBuff() {
        //Similarly to sub-point a),
        // create a test that will check which team has more strength after an additional reinforcement of both teams

        /*given*/
        Arrays.stream(villainsList).forEach(villain -> {
            try {
                redTeam.addHeroToTeam(villain);
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testStrongestTeamAfterTeamStatsBuff failed to add villain to the team");
            }
        });
        Arrays.stream(heroesList).forEach(hero -> {
            try {
                greenTeam.addHeroToTeam(hero);
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testStrongestTeamAfterTeamStatsBuff failed to add hero to the team");
            }
        });

        /*then*/
        assertEquals(null, TeamUtils.getStrongestTeam(greenTeam, redTeam));

        try {
            /*when*/
            greenTeam.increaseTeamPowerWithAdditionalBuff();
        } catch (InvalidHeroTeamException e) {
            e.printStackTrace();
            fail("testStrongestTeamAfterTeamStatsBuff failed");
        }

        /*then*/
        assertEquals(greenTeam, TeamUtils.getStrongestTeam(greenTeam, redTeam));

    }

    @Test
    public void testUpdateTeamSide() {

        //given
        superHero = HeroCreator.createHero(HeroType.HERO, "Superman", statsBig, TeamType.GREEN);
        villain = HeroCreator.createHero(HeroType.VILLAIN, "Magneto", statsSmall, TeamType.GREEN);

        //when
        Side evil = null;
        Side good = null;

        try {
            greenTeam.addHeroToTeam(villain);
            evil = greenTeam.getSide();

            greenTeam.addHeroToTeam(superHero);
            good = greenTeam.getSide();
        } catch (InvalidHeroTeamException e) {
            e.printStackTrace();
            fail("testUpdateTeamSide failed");
        }

        //then
        assertEquals(Side.EVIL, evil);
        assertEquals(Side.GOOD, good);
    }

    @Test
    public void testTeamDoNotBuffTwice() {
        //TODO refactor
        //given
        Arrays.stream(heroesList).forEach(hero -> {
            try {
                greenTeam.addHeroToTeam(hero);
            } catch (InvalidHeroTeamException e) {
                e.printStackTrace();
                fail("testTeamDoNotBuffTwice failed to add hero to the team");
            }
        });

        long originalPower;
        long powerAfter1stBuff = 0;
        boolean buffIsFalse = greenTeam.isBuff();

        originalPower = (long) greenTeam.getTeamPower();

        try {
            //when
            greenTeam.increaseTeamPowerWithAdditionalBuff();
            powerAfter1stBuff = (long) greenTeam.getTeamPower();
            //then
            assertFalse(buffIsFalse);
            assertTrue(greenTeam.isBuff());
            assertTrue(powerAfter1stBuff > originalPower);
            greenTeam.increaseTeamPowerWithAdditionalBuff();
            fail("testTeamDoNotBuffTwice passed");
        } catch (InvalidHeroTeamException e) {
            e.printStackTrace();

        }

//        assertEquals(powerAfter1stBuff, powerAfter2ndBuff);

    }

    @Test(expected = InvalidHeroTeamException.class)
    public void testInvalidHeroTeamException() throws InvalidHeroTeamException{

            greenTeam.addHeroToTeam(villain1);

    }

    @Test
    public void testGetTeamOf(){

        //given
        int teamSize = 0;
        Team team;

        //when
        try {
            team = Team.getTeamOf(5,TeamType.GREEN);
            team.getHeroesList().stream().forEach(hero -> System.out.println(hero));
            teamSize = team.getHeroesList().size();
        } catch (InvalidHeroTeamException e) {
            e.printStackTrace();
        }

        //then
        assertEquals(5,teamSize);
    }
}
