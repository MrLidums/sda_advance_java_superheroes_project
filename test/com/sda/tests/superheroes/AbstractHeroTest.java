package com.sda.tests.superheroes;

import com.sda.superheroes.HeroStatistics;
import com.sda.superheroes.SuperHero;
import com.sda.superheroes.Villain;
import com.sda.teams.Side;
import com.sda.teams.TeamType;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AbstractHeroTest {

    HeroStatistics stats;

    @Before
    public void setup() {
        stats = new HeroStatistics(100, 20, 50);
    }

    @Test
    public void testSuperHeroClass() {
        //given
//        HeroStatistics stats = new HeroStatistics(100,100,100);
        //when
        SuperHero batman = new SuperHero("Batman", stats, TeamType.GREEN);
        //then
        assertEquals("Batman", batman.getName());
        assertEquals(100, batman.getStats().getDefense());
        assertEquals(TeamType.GREEN, batman.getTeam());
        assertEquals(12000, batman.getPower(), 0);
    }

    @Test
    public void testVillainClass() {
        //given
//        HeroStatistics stats = new HeroStatistics(100,100,100);
        //when
        Villain jocker = new Villain("Jocker", stats, TeamType.RED);
        //then
        assertEquals("Jocker", jocker.getName());
        assertEquals(50, jocker.getStats().getDefense());
        assertEquals(TeamType.RED, jocker.getTeam());
        assertEquals(8500, jocker.getPower(), 0);
    }

    @Test
    public void testSideBelongTo() {

        //given
        SuperHero batman = new SuperHero("Batman", stats, TeamType.GREEN);
        Villain jocker = new Villain("Jocker", stats, TeamType.RED);

        //when
        boolean good = Side.GOOD.belongsTo(batman);
        boolean evil = Side.EVIL.belongsTo(jocker);
        boolean rightSide = Side.EVIL.belongsTo(batman);

        //then
        assertTrue(good);
        assertTrue(evil);
        assertFalse(rightSide);

    }

}

